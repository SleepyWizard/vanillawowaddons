## Interface: 11200
## Author: Naturfreund
## Title: [|cff3fcf26K|r] Natur ECB (5.4.8)
## Notes: Shows a replication of your targets cast bar, RaidBossCooldowns and Debuff-Timers like 'Sap', 'Burning Adrenaline' etc.
## Version: 5.4.8
## OptionalDeps: myAddOns
## SavedVariablesPerCharacter: CEnemyCastBar
CEnemyCastBar.xml